import os
from typing import Iterable

import pymol2
from pymol import CmdException

import settings
from pocketclustering.utils import execute_tasks
from pocketclustering.file_manager import file_manager as fm


def download_similars(pdb_ids: Iterable[str]) -> None:
    execute_tasks(pdb_ids, settings.IS_MP_DOWNLOADING, download_cifs)


def download_cifs(pdb_id: str) -> None:
    with pymol2.PyMOL() as pymol_inst:
        pymol_inst.cmd.feedback('disable', 'all', 'everything')
        cur_pwd = os.getcwd()
        pymol_inst.cmd.set('connect_mode', 4)
        pymol_inst.cmd.cd(fm.get_folder_cif())
        try:
            pymol_inst.cmd.fetch(pdb_id, async_=0)
        except CmdException:
            # if you care about trying to overwrite downloading files, use IS_MP_DOWNLOADING = False
            pass
        pymol_inst.cmd.cd(cur_pwd)

    return None
