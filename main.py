import argparse
import os

from pocketclustering.alignment import write_align_result_file
from pocketclustering.download import download_similars
from pocketclustering.file_manager import make_output_suffix
from pocketclustering.fg_search import search_functional_groups, get_similars, SearchInput
from pocketclustering.minimization import prepare_pdb_for_amber, minimize_clusters, check_amber_binaries
from pocketclustering.clustering import clustering, write_pse_session_with_clusters
from pocketclustering.functional_groups import init_functional_groups, write_functional_groups_info
from pocketclustering.objects import TargetObject

import settings
from pocketclustering.utils import read_catalog


def apply_arguments(args: argparse.Namespace):
    if (settings.FIND_ONLY_IN_SELF_CHAIN is None) and (settings.NOT_FIND_IN_SELF_CHAIN is None):
        settings.FIND_ONLY_IN_SELF_CHAIN = args.self_chain_only
        settings.NOT_FIND_IN_SELF_CHAIN = args.not_self_chain

        if settings.FIND_ONLY_IN_SELF_CHAIN and settings.NOT_FIND_IN_SELF_CHAIN:
            raise Warning('You cannot pass self_chain_only and not_self_chain flags at the same time (empty result)')
        elif args.self_chain_only:
            raise Warning('FIND_ONLY_IN_SELF_CHAIN and NOT_FIND_IN_SELF_CHAIN in settings.py must be None if you want to use self_chain_only or not_self_chain flag')

    if settings.FIND_IN_LIGAND is None:
        settings.FIND_IN_LIGAND = not args.no_ligand
    elif args.no_ligand:
        raise Warning('FIND_IN_LIGAND in settings.py must be None if you want to use no_ligand flag')

    if settings.FIND_IN_PROTEIN is None:
        settings.FIND_IN_PROTEIN = not args.no_protein
    elif args.no_protein:
        raise Warning('FIND_IN_PROTEIN in settings.py must be None if you want to use no_protein flag')

    if settings.USE_MINIMIZATION is None:
        settings.USE_MINIMIZATION = args.minimization
    elif args.minimization:
        raise Warning('USE_MINIMIZATION in settings.py must be None if you want to use minimization flag')

    if settings.UPPER_IDENTITY_BOUND is None:
        settings.UPPER_IDENTITY_BOUND = args.max_ident
    elif args.max_ident:
        raise Warning('UPPER_IDENTITY_BOUND in settings.py must be None if you want to use minimization flag')

    if settings.CAT_SMILES_CUSTOM is None:
        if args.lig_expo is not None:
            settings.CAT_SMILES_CUSTOM = read_catalog(args.lig_expo)
    elif args.max_ident:
        raise Warning('CAT_SMILES_CUSTOM in settings.py must be None if you want to use lig_expo flag')


def main(args: argparse.Namespace):
    exclusion_list = [x.upper() for x in args.exclude_pdb_id]
    output_suffix = make_output_suffix(args.smarts_catalog)
    input_identity = args.identity
    functional_groups_catalog = init_functional_groups(args.smarts_catalog, settings.FORCE_UNIQUE_FGROUPS_NAMES)
    target_obj = TargetObject(args.pdb_id_or_file, args.chain, args.volume_file)

    amber_compatible_pdb = None
    if settings.USE_MINIMIZATION:
        check_amber_binaries()
        amber_compatible_pdb = prepare_pdb_for_amber(target_obj)

    print(f'Program start for {target_obj.pdb_id}, chain {target_obj.chain}')
    align_suffix, similars = get_similars(args.align_data, target_obj, input_identity, settings.E_VALUE_CUTOFF)
    output_name = f'{target_obj.get_full_name()}_{output_suffix}({input_identity})_{align_suffix}'

    finding_input = []
    for similar_object, alignment in similars:
        finding_input.append(SearchInput(target_obj, similar_object, alignment, functional_groups_catalog))
    finding_input = list(filter(lambda x: x.similar_object.pdb_id not in exclusion_list, finding_input))

    pdb_ids_to_download = set(fi.similar_object.pdb_id for fi in list(filter(lambda o: o.similar_object.custom_file_path is None, finding_input)))
    download_similars(pdb_ids_to_download)
    print('All cif files downloaded')

    finding_data = search_functional_groups(finding_input)

    print('Functional groups clustering_task:')
    write_align_result_file(finding_data.align_info_data, output_name)
    write_functional_groups_info(finding_data.fgroups_info_data, output_name)
    clustering_input = finding_data.similar_output_list

    clusters = clustering(clustering_input)
    write_pse_session_with_clusters(target_obj, clusters, output_name)

    if settings.USE_MINIMIZATION:
        minimize_clusters(clusters, target_obj, output_name, amber_compatible_pdb)

    if amber_compatible_pdb is not None:
        os.unlink(amber_compatible_pdb)
    target_obj.finish()


parser = argparse.ArgumentParser(description='Perform pocket clustering_task and create PyMOL session with results')
parser.add_argument('pdb_id_or_file', type=str, default=None,
                    help='PDB ID of the target or path to pdb file with target protein')
parser.add_argument('chain', type=str,
                    help='Chain ID of the target')
parser.add_argument('identity', type=int,
                    help='Required identity for similarity search (integer 0-100)')
parser.add_argument('volume_file', type=str,
                    help='Volume file (.pdb) describing pocket of interest')
parser.add_argument('smarts_catalog', type=str,
                    help='File with smarts catalog.')
parser.add_argument('align_data', nargs='?', default=None, type=str,
                    help='File with alignment data for target protein (BlastP .xml or .hhr). If empty, BLAST will be used to download new alignment data file')
parser.add_argument('-sco', '--self_chain_only', action='store_true',
                    help='If provided, the search of functional groups will be performed only in self chain')
parser.add_argument('-nsc', '--not_self_chain', action='store_true',
                    help='If provided, the search of functional groups will not be performed in self chain')
parser.add_argument('-nl', '--no_ligand', action='store_true',
                    help='If provided, the search will not be performed in ligand')
parser.add_argument('-np', '--no_protein', action='store_true',
                    help='If provided, the search will not be performed in protein')
parser.add_argument('-min', '--minimization', action='store_true',
                    help='If provided, minimization will be performed')
parser.add_argument('-uib', '--max_ident', action='store', default=100, type=int, nargs='?',
                    help='If provided,  define upper bound of identity')
parser.add_argument('-ex', '--exclude_pdb_id', action='append', default=[], type=str, nargs='?',
                    help='Сan be transmitted provided times, define exclusion list')
parser.add_argument('-lexp', '--lig_expo', action='store', default=None, type=str, nargs='?',
                    help='If provided, used to parameterize the ligands with resn "lig", by the name of the file')
arguments = parser.parse_args()

apply_arguments(arguments)  # Important to call apply_arguments() in global scope

if __name__ == '__main__':  # confirms that the code is under main function

    main(arguments)
