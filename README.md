# Pocket clustering
Application for searching and clustering functional groups in protein pockets.

## Usage
    python main.py pdb_id_or_file chain identity volume_file smarts_catalog [align_data] [-h] [-sco] [-nsc] [-nl] [-np]

### Positional arguments:

    pdb_id_or_file      PDB ID of the target or path to pdb file with target
    chain               Chain ID of the target
    identity            Required identity for similarity search (0-100)
    volume_file         Volume file (.pdb) describing pocket of interest
    smarts_catalog      File with smarts catalog.
    align_data          File with alignment data for target protein (BlastP .xml or HHPred .hhr). 
                        If empty, BLAST will be used to download new alignment data file

### Optional arguments:

    -h,     --help                  show this help message and exit
    -sco,    --self_chain_only      If provided, the search of functional groups will be performed 
                                    only in self chain of each simmilar protein
    -nsc,    --not_self_chain       If provided, the search of functional groups will not be performed 
                                    in self chain of each simmilar protein
    -nl,    --no_ligand             If provided, the search will not be performed in ligand
    -np,    --no_protein            If provided, the search will not be performed in protein
    -min    --minimization          If provided, minimization will be performed
    -uib [MAX_IDENT]    /
    --max_ident [MAX_IDENT]         If provided, define upper bound of identity
    -ex [PDB_ID] /
    --exclude_pdb_id [PDB_ID]       Can be provided multiple times, define exclusion list
  
### Examples: 

    #   Simple launch for protein from Protein Data Bank
    python main.py 3GFT A 100 pockets/3gft_1.pdb smarts/row.tsv 
    
    #   Simple launch for protein from Protein Data Bank and minimization
    python main.py 3GFT A 100 pockets/3gft_1.pdb smarts/row.tsv  -min
    
    #   Simple launch for protein from Protein Data Bank with 2 excluded pdb
    python main.py 3GFT A 100 pockets/3gft_1.pdb smarts/row.tsv  -min -ex 3GFT -ex 4DSO
    
    #   Simple launch for protein from pdb file
    python main.py custom_protein.pdb A 70 pockets/3gft_1.pdb smarts/row.tsv 
    
    #   Launch searching of functional groups only within self chain
    python main.py 3GFT A 30 pockets/3gft_1.pdb smarts/row.tsv align_data/target_3GFTA.xml --self_chain_only
    
    #   Launch searching of functional groups in the ligands outside their own chain
    python main.py 3GFT A 30 pockets/3gft_1.pdb smarts/row.tsv align_data/target_3GFTA.xml --not_self_chain --no_protein
    

## Installation
Required Python 3.7 and following packages. All these packages are installed as part of Conda environment:

- PyMOL
- RDKit
- Biopython
- Jinja2
- AMBER (for minimization)
- Open Babel


  
Important (Linux):

- libGL
- libxrender
  
### Downloading
    git clone https://bitbucket.org/abc-group/pocketclustering.git
    cd pocketclustering

### Miniconda installation
For Windows(exe installer) and Mac(.pkg installer): 
    
- Download and run installer from: https://conda.io/en/latest/miniconda.html

For Linux x64:
    
	curl -O https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
	bash Miniconda3-latest-Linux-x86_64.sh
    exec bash

### Environment installation
    conda env create -f "conda-env.yml" -n pocket_clustering
    
### Amber information

Amber is used for minimization. Amber will be installed automatically if you use *conda-env.yml*
Do not use the `--minimize` flag, if Amber is not installed.
    
We strongly recommend using version 18 to avoid errors.
In the case of changing the Amber installation location, modify the variable `AMBER_FOLDER` in *settings.py*

## How to get fasta for HHPred and Blast using pymol?
Pymol shell:

    fetch 3GFTA
    print cmd.get_fastastr("3GFTA and polymer and present")