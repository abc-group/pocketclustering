import csv
from abc import abstractmethod
from enum import Enum
from pathlib import Path
from typing import List, Optional, Tuple, Iterable

import pymol2
from Bio.Blast import NCBIXML
from Bio.Blast.Record import HSP
from Bio.SubsMat.MatrixInfo import blosum62 as blosum62_matrix

import settings
from pocketclustering.objects import SimilarObject, TargetObject
from pocketclustering.thirdparty.hh_reader import hhr_alignment
from pocketclustering.thirdparty.pymol import NothingToAlignException, pymol_focus_alignment
from pocketclustering.thirdparty import hh_reader
from pocketclustering.file_manager import file_manager as fm


class Align(Enum):
    MATCH = 1
    POSITIVE = 2
    NEGATIVE = 3
    INDEL_Q = 4
    INDEL_S = 5
    OUT = 6
    NOT_INDEL_Q = 7
    EMPTY = 8

    def __repr__(self):
        return '<%s.%s>' % (self.__class__.__name__, self.name)


class BaseAlign:
    identity: Optional[float]
    pocket_rmsd: Optional[float]
    target_obj: TargetObject
    resi_to_align: List[str]
    indels_q: int
    al_pos: int
    al_neg: int
    al_match: int
    al_indel_s: int
    al_indel_q: int
    al_out_of_al: int

    @abstractmethod
    def __init__(self, target_obj: TargetObject):
        self.resi_to_align = []
        self.target_obj = target_obj
        self.pocket_rmsd = None
        self.identity = None
        self.indels_q = 0
        self.al_pos = 0
        self.al_neg = 0
        self.al_match = 0
        self.al_indel_s = 0
        self.al_indel_q = 0
        self.al_out_of_al = 0

    @abstractmethod
    def check_match(self, q: str, m: str, s: str) -> Align:
        pass

    def process_indels(self, q_str: str, s_str: str, q_start: int, q_end: int, m_str: Optional[str] = None) -> Tuple[List[Align], List[Align]]:
        if m_str is None:
            m_str = q_str
        fasta_str_small = ''
        match_arr = []
        indelq_arr = []
        for q, m, s in zip(q_str, m_str, s_str):
            if q == '-':
                self.indels_q += 1
            if q != '-':
                fasta_str_small += q
                match_arr.append(self.check_match(q, m, s))

                indelq_arr.append(self.indels_q)

        fasta_str = self.target_obj.fasta

        if fasta_str.find(fasta_str_small) == -1:
            print(fasta_str)
            print(fasta_str_small)
            raise Exception("Fasta of target obj_name did not match to fasta in sim obj_name")

        match_arr = _extend_align(q_start, match_arr, len(fasta_str) - q_end - 1)
        indelq_arr = _extend_indelq_array(q_start, indelq_arr, len(fasta_str) - q_end - 1)
        res_indelq_str = indels_array_to_string(indelq_arr)

        return match_arr, res_indelq_str

    def initialize(self, match_str: List[Align], res_indelq_str: List[Align]) -> None:
        resi_in_pocket = self.target_obj.resi_in_pocket
        pok_str = [Align.EMPTY] * len(match_str)
        pok_indelq_str = [Align.EMPTY] * len(match_str)
        for resi in resi_in_pocket:
            ind = self.target_obj.resi_to_ind[resi]
            if match_str[ind] == Align.MATCH:
                self.resi_to_align.append(resi)
            pok_str[ind] = match_str[ind]
            pok_indelq_str[ind] = res_indelq_str[ind]

        self.al_pos = pok_str.count(Align.POSITIVE)
        self.al_neg = pok_str.count(Align.NEGATIVE)
        self.al_match = pok_str.count(Align.MATCH)
        self.al_indel_s = pok_str.count(Align.INDEL_S)
        self.al_indel_q = pok_indelq_str.count(Align.INDEL_Q)
        self.al_out_of_al = pok_str.count(Align.OUT)

    def align_structures(self, pymol_inst: pymol2.PyMOL, target_obj_name: str, similar_obj_name: str) -> None:
        if len(self.resi_to_align) == 0:
            self.pocket_rmsd = None
            return

        resi_str = 'resi ' + '+'.join(self.resi_to_align)
        pymol_align_target = f'{target_obj_name} and ({resi_str})'

        selection_on_target = 'obj_t'
        pymol_inst.cmd.select(selection_on_target, pymol_align_target)
        try:
            self.pocket_rmsd = self.align_function(pymol_inst, similar_obj_name, target_obj_name, selection_on_target)
        except NothingToAlignException:
            self.pocket_rmsd = None

    @abstractmethod
    def align_function(self, pymol_inst: pymol2.PyMOL, similar_obj_name: str, target_obj_name: str, selection_on_target: str) -> float:
        pass


class BlastAlign(BaseAlign):
    def __init__(self, target_obj: TargetObject, hsp: HSP):
        super().__init__(target_obj)
        self.identity = float(hsp.identities) / float(hsp.align_length) * 100

        match_str, res_indelq_str = self.process_indels(hsp.query, hsp.sbjct, hsp.query_start - 1, hsp.query_end - 1, m_str=hsp.match)
        self.initialize(match_str, res_indelq_str)

    def check_match(self, q: str, m: str, s: str) -> Align:
        if s == '-':
            return Align.INDEL_S
        else:
            if m == '+':
                return Align.POSITIVE
            elif m == ' ':
                return Align.NEGATIVE
            else:
                return Align.MATCH

    def align_function(self, pymol_inst, similar_obj_name, target_obj_name, selection_on_target) -> float:
        return pymol_focus_alignment(pymol_inst, similar_obj_name, target_obj_name, selection_on_target)


class HhpredAlign(BaseAlign):
    def __init__(self, target_obj: TargetObject, hh_obj: hhr_alignment):
        super().__init__(target_obj)
        self.identity = hh_obj.identity * 100
        match_str, res_indelq_str = self.process_indels(''.join(hh_obj.query_ali), ''.join(hh_obj.template_ali),
                                                        hh_obj.start[0] - 1, hh_obj.end[0] - 1)
        self.initialize(match_str, res_indelq_str)

    def check_match(self, q: str, m: str, s: str) -> Align:
        if q == '-':
            return Align.INDEL_Q
        elif s == q:
            return Align.MATCH
        elif s == '-':
            return Align.INDEL_S
        else:
            pair = (q, s)
            if pair not in blosum62_matrix:
                value = blosum62_matrix[(tuple(reversed(pair)))]
            else:
                value = blosum62_matrix[pair]
            return Align.POSITIVE if value > 0 else Align.NEGATIVE

    def align_function(self, pymol_inst: pymol2.PyMOL, similar_obj_name: str, target_obj_name: str, selection_on_target: str) -> float:
        return pymol_focus_alignment(pymol_inst, similar_obj_name, target_obj_name, selection_on_target)


class CustomAlign(BaseAlign):
    def __init__(self, target_obj: TargetObject):
        super().__init__(target_obj)

    def check_match(self, q: str, m: str, s: str) -> Align:
        pass

    def align_function(self, pymol_inst: pymol2.PyMOL, similar_obj_name: str, target_obj_name: str, selection_on_target: str = '') -> float:
        return pymol_inst.cmd.align(similar_obj_name, target_obj_name)[0]


class AlignmentResult:
    similar_object: SimilarObject
    alignment: BaseAlign
    is_used: bool

    def __init__(self, similar_object: SimilarObject, alignment: BaseAlign, is_used: bool):
        self.similar_object = similar_object
        self.alignment = alignment
        self.is_used = is_used


def _extend_align(length1: int, string: List[Align], length2: int) -> List[Align]:
    return [Align.OUT] * length1 + string + [Align.OUT] * length2


def _extend_indelq_array(length1: int, array: List[int], length2: int) -> List[int]:
    result = [0] * length1 + array + [array[-1]] * length2

    return result


def indels_array_to_string(indel_arr: List[int]) -> List[Align]:
    result = []
    for i, q_i in enumerate(indel_arr):
        q_prev = indel_arr[max(0, i - 1)]
        q_next = indel_arr[min(len(indel_arr) - 1, i + 1)]

        if q_i != q_prev or q_i != q_next:
            result += [Align.INDEL_Q]
        else:
            result += [Align.NOT_INDEL_Q]
    return result


def parse_blast_align_file(filename: str, target_obj: TargetObject, identity_cutoff: float) -> List[(Tuple[SimilarObject, BlastAlign])]:
    similars = []
    with open(filename) as similars_fp:
        sim_id = 1
        blast_records = NCBIXML.parse(similars_fp)
        for record in blast_records:
            for align in record.alignments:
                altsp = align.title.strip().split('gi|')
                sim_names = [t.split('|')[2] for t in altsp[1:]]
                sim_chains_row = [t.split('|')[3].split(' ')[2] for t in altsp[1:]]
                sim_chains = [sim_chain.rstrip(',') for sim_chain in sim_chains_row]
                for hsp in align.hsps:
                    identity_perc = float(hsp.identities) / float(hsp.align_length) * 100
                    if settings.UPPER_IDENTITY_BOUND >= identity_perc >= identity_cutoff:
                        for sim_name, sim_chain in zip(sim_names, sim_chains):
                            sim_obj = SimilarObject(sim_name, sim_chain, sim_id)
                            similars.append((sim_obj, BlastAlign(target_obj, hsp)))
                            sim_id += 1
    return similars


def parse_hhpred_align_file(filename: str, target_obj: TargetObject, identity_cutoff: float) -> List[(Tuple[SimilarObject, HhpredAlign])]:
    similars = []
    hh_objs = hh_reader.read_result(filename)
    sim_id = 1
    for hh_obj in hh_objs:
        if settings.UPPER_IDENTITY_BOUND >= hh_obj.identity * 100 >= identity_cutoff:
            sim_name, sim_chain = hh_obj.template_id.split('_')
            sim_obj = SimilarObject(sim_name, sim_chain, sim_id)
            similars.append((sim_obj, HhpredAlign(target_obj, hh_obj)))
            sim_id += 1
    return similars


def parse_custom_align_file(filename: str, target_obj: TargetObject) -> List[(Tuple[SimilarObject, CustomAlign])]:
    similars = []
    with open(filename) as tsvfile:
        tsvreader = csv.reader(tsvfile, delimiter="\t")
        header = next(tsvreader, None)
        ind_file = header.index('File')
        ind_chain = header.index('Chain')
        ind_method = header.index('Method')
        ind_resolution = header.index('Resolution')
        sim_id = 1
        for line in tsvreader:
            sim_file = Path(line[ind_file])
            sim_chain = line[ind_chain]
            sim_obj = SimilarObject(sim_file, sim_chain, sim_id, is_custom_file=True)
            sim_obj.assign_header_info(line[ind_method], line[ind_resolution])
            similars.append((sim_obj, CustomAlign(target_obj)))
            sim_id += 1
    return similars


def write_align_result_file(align_info_data: Iterable[AlignmentResult], filename_prefix: str) -> None:
    sorted_align_results = sorted(align_info_data, key=lambda d: d.similar_object)
    with open(fm.get_output_file(filename_prefix, '_align.txt'), 'w') as tsvfile:
        fieldnames = ['Used', 'Name', 'Ident', 'RMSD',
                      'Positive', 'Negative', 'Match', 'Indel S', 'Indel Q', 'Out of Al', 'Structure_method',
                      'Resolution']
        writer = csv.DictWriter(tsvfile, fieldnames=fieldnames, delimiter='\t')
        writer.writeheader()

        for align_result in sorted_align_results:
            alignment = align_result.alignment
            row_data = {
                'Used': align_result.is_used,
                'Name': align_result.similar_object.get_full_name(),
                'Ident': ('%.0f' % (alignment.identity if (alignment.identity is not None) else -1)),
                'RMSD': ('%.8f' % (alignment.pocket_rmsd if (alignment.pocket_rmsd is not None) else -1)),
                'Positive': alignment.al_pos,
                'Negative': alignment.al_neg,
                'Match': alignment.al_match,
                'Indel S': alignment.al_indel_s,
                'Indel Q': alignment.al_indel_q,
                'Out of Al': alignment.al_out_of_al,
                'Structure_method': align_result.similar_object.get_structure_method(),
                'Resolution': align_result.similar_object.get_resolution()
            }
            writer.writerow(row_data)
