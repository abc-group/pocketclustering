import os
from collections import defaultdict
from typing import List, Tuple, DefaultDict, Optional, Iterable

import pymol2
from Bio.Blast import NCBIWWW
from rdkit.Chem.rdchem import Mol

import settings
from pocketclustering.alignment import parse_blast_align_file, parse_hhpred_align_file, parse_custom_align_file, AlignmentResult, BaseAlign
from pocketclustering.functional_groups import FunctionalGroup, FunctionalGroupMatcher
from pocketclustering.objects import TargetObject, SimilarObject
from pocketclustering.pymol import pymol_reset, mol_to_pymol
from pocketclustering.residues import LocationMark, load_residue_from_mol, SanitizeError, ResidueObject
from pocketclustering.utils import execute_tasks
from pocketclustering.file_manager import file_manager as fm


class SearchInput:
    target_obj: TargetObject
    similar_object: SimilarObject
    alignment: BaseAlign
    smarts_catalog: List[FunctionalGroupMatcher]

    def __init__(self, target_obj: TargetObject, similar_object: SimilarObject, alignment: BaseAlign, smarts_catalog: List[FunctionalGroupMatcher]):
        self.target_obj = target_obj
        self.similar_object = similar_object
        self.alignment = alignment
        self.smarts_catalog = smarts_catalog


class SearchOutput:
    fg_name: str
    smiles: str
    list_mol_adv: List[Tuple[Mol, str]]

    def __init__(self, fg_name: str, smiles: str, list_mol_adv: List[Tuple[Mol, str]]):
        self.fg_name = fg_name
        self.smiles = smiles
        self.list_mol_adv = list_mol_adv


class SearchResult:
    fgroups_info_data: DefaultDict[str, List[str]]
    similar_output_list: List[SearchOutput]
    align_info_data: List[AlignmentResult]

    def __init__(self, fgroups_info_data: DefaultDict[str, List[str]], similar_output_list: List[SearchOutput], align_info_data: List[AlignmentResult]):
        self.fgroups_info_data = fgroups_info_data
        self.similar_output_list = similar_output_list
        self.align_info_data = align_info_data


class SearchInResidueResult:
    functional_groups: List[FunctionalGroup]
    residue: ResidueObject
    similar_object: SimilarObject
    similar_is_used: bool
    errors_data: List[str]

    def __init__(self, functional_groups: List[FunctionalGroup], residue: ResidueObject, similar_object: SimilarObject, similar_is_used: bool, errors_data: List[str]):
        self.functional_groups = functional_groups
        self.residue = residue
        self.similar_object = similar_object
        self.similar_is_used = similar_is_used
        self.errors_data = errors_data


def get_similars(align_file: Optional[str], target_obj: TargetObject, identity_cut_off: float, e_value: float) -> Tuple[str, List[(Tuple[SimilarObject, BaseAlign])]]:
    if settings.DEBUG:
        print('Fasta:')
        print(target_obj.fasta)

    if align_file is None:
        align_file = f'{fm.get_folder_align_data()}/{target_obj}.xml'
        print('Downloading similar list')
        result_handle = NCBIWWW.qblast('blastp', 'pdb', target_obj.fasta,
                                       hitlist_size=settings.MAX_HITLIST_SIZE, expect=e_value)
        with open(align_file, 'w') as out_handle:
            out_handle.write(result_handle.read())
        result_handle.close()

    if not os.path.exists(align_file):
        raise FileNotFoundError(f"Align file {align_file} does not exist")

    print('Geting similars in file ' + align_file)
    ext = os.path.splitext(os.path.basename(align_file))[-1]
    if ext == '.xml':
        similars = parse_blast_align_file(align_file, target_obj, identity_cut_off)
        align_method_name = "blast"
    elif ext == '.hhr':
        similars = parse_hhpred_align_file(align_file, target_obj, identity_cut_off)
        align_method_name = "HHpred"
    elif ext == '.txt':
        similars = parse_custom_align_file(align_file, target_obj)
        align_method_name = "custom"
    else:
        raise ValueError(f'Alignment files with {ext} extension is not supported')

    print(f"Detected {align_method_name}")
    return align_method_name, similars


def verify_not_pocket(pymol_inst: pymol2.PyMOL, target_obj: TargetObject, res_obj: ResidueObject) -> bool:
    if not res_obj.is_ligand:
        sel_is_pocket = f'({target_obj} and name ca and resn {res_obj.location.resn}) near_to {settings.MIN_DST_OVERLAY} of ' \
                        f'({res_obj.location.obj_name} and name ca and resi {res_obj.location.resi})'
        # Not pocket, if СА atoms far from each other
        return pymol_inst.cmd.select('sel_is_pocket', selection=sel_is_pocket) == 0
    else:
        return True


def verify_settings_filter(res_obj: ResidueObject) -> bool:
    if settings.FIND_ONLY_IN_SELF_CHAIN and res_obj.location.location_mark != LocationMark.self_chain:
        return False
    if settings.NOT_FIND_IN_SELF_CHAIN and res_obj.location.location_mark == LocationMark.self_chain:
        return False
    if res_obj.is_ligand:
        return settings.FIND_IN_LIGAND
    else:
        return settings.FIND_IN_PROTEIN


def verify_structure(sim_obj: SimilarObject) -> bool:
    resolution = sim_obj.get_resolution()
    if resolution is not None:
        if resolution > settings.RESOLUTION_CUTOFF:
            return False
    if sim_obj.get_structure_method() in settings.BAD_METHODS:
        return False
    return True


def verify_location(pymol_inst: pymol2.PyMOL, target_obj: TargetObject, res_obj: ResidueObject) -> bool:
    return (res_obj.location.location_mark != LocationMark.self_chain) or verify_not_pocket(pymol_inst, target_obj, res_obj)


def check_structure_alignment(alignment: BaseAlign) -> bool:
    return alignment.pocket_rmsd <= settings.POCKET_RMSD_CUT


def search_functional_groups(search_input: Iterable[SearchInput]) -> SearchResult:
    return execute_tasks(search_input, settings.IS_MP_FINDING, search_functional_groups_task, parse_search_result)


def search_in_residue(pymol_inst: pymol2.PyMOL, target_obj: TargetObject, sim_obj: SimilarObject, res_obj: ResidueObject, functional_groups_catalog: Iterable[FunctionalGroupMatcher]) -> SearchInResidueResult:
    similar_obj_is_used = False
    res_mol_file = fm.get_mol_file_residue(sim_obj, target_obj, str(res_obj))
    pymol_inst.cmd.save(res_mol_file, selection=f'{res_obj.location.obj_name} and chain {res_obj.location.chain} and resi {res_obj.location.resi}')
    functional_groups = []
    errors_data = []
    try:
        if res_obj.location.resn == 'LIG' and settings.CAT_SMILES_CUSTOM is not None:
            print(settings.CAT_SMILES_CUSTOM[sim_obj.pdb_id])
            res_mol = load_residue_from_mol(res_mol_file, smiles=settings.CAT_SMILES_CUSTOM[sim_obj.pdb_id], try_default_sanitization=False)
        else:
            res_mol = load_residue_from_mol(res_mol_file, resn=res_obj.location.resn, try_default_sanitization=(not res_obj.is_ligand))
        for fg in functional_groups_catalog:
            if fg.has_match(res_mol):
                similar_obj_is_used = True
                for match in fg.get_matches(res_mol):
                    mol_to_pymol(pymol_inst, res_mol, 'temp_fg')
                    atoms_in_match_str = ' or '.join('id %d' % (at + 1) for at in match)
                    sel_str = f'temp_fg and ( {atoms_in_match_str} )'
                    if settings.CONSIDER_FG_ONLY_IN_VOL:
                        sel_in_vol = f'({sel_str}) within {settings.VOLUME_RADIUS} of {settings.VOLUME_OBJ}'
                        num_atoms_in_vol = pymol_inst.cmd.select('sel_in_vol', selection=sel_in_vol)
                        in_vol = (num_atoms_in_vol != 0)
                    else:
                        in_vol = True

                    if in_vol:
                        functional_groups.append(FunctionalGroup(pymol_inst, sel_str, fg.name))

                    pymol_inst.cmd.delete('temp_fg')

    except SanitizeError as e:
        errors_data.append(f'Error: {e}')

    return SearchInResidueResult(functional_groups, res_obj, sim_obj, similar_obj_is_used, errors_data)


def search_functional_groups_task(search_input: SearchInput) -> Tuple[List[SearchInResidueResult], AlignmentResult]:
    search_result = []
    sim_obj = search_input.similar_object
    alignment = search_input.alignment
    target_obj = search_input.target_obj
    with pymol2.PyMOL() as pymol_inst:
        pymol_inst.cmd.feedback('disable', 'all', 'everything')
        pymol_inst.cmd.feedback('enable', 'all', 'errors warnings')
        pymol_reset(pymol_inst, target_obj.path_to_pse)
        sim_obj.load_in_pymol(pymol_inst)
        if sim_obj.get_structure_method() is None:
            sim_obj.load_header_info()
        alignment.align_structures(pymol_inst, target_obj.get_pymol_name(), sim_obj.get_pymol_name())
        print(f'({sim_obj.get_id()}) {sim_obj} ({int(alignment.identity)}%):')

        align_is_good = (alignment.pocket_rmsd is not None) and check_structure_alignment(alignment)
        if align_is_good and verify_structure(sim_obj):
            sim_obj.extend(pymol_inst)
            res_near_volume = sim_obj.find_residues_in_volume(pymol_inst, settings.VOLUME_OBJ, settings.VOLUME_RADIUS)

            for res_obj in res_near_volume:
                if verify_location(pymol_inst, target_obj, res_obj) and verify_settings_filter(res_obj):
                    res_info = search_in_residue(pymol_inst, target_obj, sim_obj, res_obj, search_input.smarts_catalog)
                    search_result.append(res_info)

    alignment_result = AlignmentResult(sim_obj, alignment, any([res.similar_is_used for res in search_result]))

    return search_result, alignment_result


def parse_search_result(result_data: Iterable[SearchOutput]) -> SearchResult:
    fgroups_info_data = defaultdict(list)
    fgroups_clustering_data = defaultdict(list)
    align_info_data = []
    for search_result, alignment_result in result_data:
        align_info_data.append(alignment_result)

        for fg_info in search_result:
            res_obj = fg_info.residue
            functional_groups = fg_info.functional_groups
            list_errors = fg_info.errors_data

            for fg in functional_groups:
                residue_location = res_obj.location
                fgroups_info_data[fg.full_name].append(residue_location.to_json())
                fg_chem_name = (fg.name, fg.smiles_escaped)
                fgroups_clustering_data[fg_chem_name].append([fg.mol, residue_location])
            for error in list_errors:
                residue_location = res_obj.location
                fgroups_info_data[error].append(residue_location.to_json())

    similar_output_list = []
    for key, value in fgroups_clustering_data.items():
        fg_name, smiles = key
        similar_output_list.append(SearchOutput(fg_name, smiles, value))

    return SearchResult(fgroups_info_data, similar_output_list, align_info_data)
