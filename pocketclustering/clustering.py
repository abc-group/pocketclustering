import random
from typing import List, Tuple, Optional, Iterable

import pymol2
from rdkit.Chem.rdchem import Mol
from rdkit.ML.Cluster import Butina

import settings
from pocketclustering.fg_search import SearchOutput
from pocketclustering.objects import TargetObject
from pocketclustering.pymol import mol_to_pymol, pymol_atomic_color, pymol_reset, pymol_color_target_with_volume, pymol_save
from pocketclustering.utils import gen_sym_maps, calc_rmsd, escape_chars, execute_tasks
from pocketclustering.file_manager import file_manager as fm


class ClusterElement:
    def __init__(self, name: str, mol: Mol):
        self.name = name
        self.mol = mol


class Cluster:
    def __init__(self, name: str, cluster_id: int, list_mol_adv: List[Tuple[Mol, str]], cluster_elements_id: Tuple[int, ...]):
        self.name = name
        self.cluster_id = cluster_id
        self.members = []
        for element_id in cluster_elements_id:
            mol, element_name = list_mol_adv[element_id]
            self.members.append(ClusterElement(element_name, mol))

    def __len__(self):
        return len(self.members)

    def __str__(self):
        return f'{self.name}.{self.cluster_id}_{len(self)}'

    def get_centroid(self) -> ClusterElement:
        return self.members[0]

    def load_to_pymol(self, pymol_inst: pymol2.PyMOL, only_centroid: bool = True) -> None:
        elements = [self.get_centroid()] if only_centroid else self.members

        for i, element in enumerate(elements):
            element_name = f'{self}.{element.name}'
            mol_to_pymol(pymol_inst, element.mol, element_name)
            pymol_atomic_color(pymol_inst, element_name, random.choice(settings.PYMOL_COLORS))
            if i != 0:
                pymol_inst.cmd.disable(element_name)
            pymol_inst.cmd.disable(self.name)


def clustering(clustering_input: Iterable[SearchOutput]) -> List[Cluster]:
    return execute_tasks(clustering_input, settings.IS_MP_CLUSTERING, clustering_task, filter_clustering_result)


def clusterize_adv_list(list_mol_adv: List[Tuple[Mol, str]], prefix: str = '') -> List[Cluster]:
    list_mol = [x[0] for x in list_mol_adv]
    clustering_result = clusterize_mol_list(list_mol)

    clusters = []
    for cluster_id, cluster_elements_id in enumerate(clustering_result):
        cluster = Cluster(prefix, cluster_id, list_mol_adv, cluster_elements_id)
        clusters.append(cluster)

    return clusters


def clusterize_mol_list(mols: List[Mol]) -> Tuple[Tuple[int, ...], ...]:
    sym_maps = gen_sym_maps(mols)
    # Calculate RMS matrix taking into account symmetry
    rms_mat = []
    for i in range(len(mols)):
        for j in range(i):
            rms_mat.append(min([calc_rmsd(mols[i], mols[j], sym_map) for sym_map in sym_maps]))

    # Use greedy Butina clusterization
    clusters = Butina.ClusterData(rms_mat, len(mols), settings.CLUSTER_RADIUS, isDistData=True, reordering=True)
    return clusters


def clustering_task(similars_output: SearchOutput) -> Optional[List[Cluster]]:
    name = similars_output.fg_name
    smile = similars_output.smiles
    list_mol_adv = similars_output.list_mol_adv
    n = len(list_mol_adv)
    print(f'Clustering functional group:\t{name}\tsmile={smile}\telements={n}')
    if n >= settings.FGROUP_SIZE_CUTOFF:
        cluster_name = escape_chars(name + '.' + smile.replace('.', '_'))
        clusters = clusterize_adv_list(list_mol_adv, prefix=cluster_name)
    else:
        return None

    clusters = filter_clusters(clusters)
    return clusters


def filter_clusters(clusters: Iterable[Cluster]) -> List[Cluster]:
    return list(filter(lambda cluster: len(cluster) >= settings.CLUSTER_SIZE_CUTOFF, clusters))


def filter_clustering_result(result: Iterable[Iterable[Cluster]]) -> List[Cluster]:
    clusters = []
    filtered_result = list(filter(lambda x: x is not None, result))
    sorted_clusters = sorted(filtered_result, key=lambda c: c[0].name)

    for result_clusters in sorted_clusters:
        for cluster in result_clusters:
            clusters.append(cluster)

    return clusters


def write_pse_session_with_clusters(target_obj: TargetObject, clusters: Iterable[Cluster], output_name: str):
    with pymol2.PyMOL() as pymol_inst:
        pymol_inst.cmd.set('group_auto_mode', 2)
        pymol_reset(pymol_inst, target_obj.path_to_pse)
        pymol_color_target_with_volume(pymol_inst, str(target_obj), settings.VOLUME_OBJ)

        for cluster in clusters:
            cluster.load_to_pymol(pymol_inst, only_centroid=False)

        pymol_save(pymol_inst, fm.get_output_file(output_name, '.pse'))
