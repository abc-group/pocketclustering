import os
import shutil
import subprocess
from typing import Optional, Iterable

import pymol2
from rdkit import Chem
from rdkit.Chem import AllChem
from rdkit.Chem.rdchem import Mol

import settings
from pocketclustering.clustering import calc_rmsd, Cluster
from pocketclustering.file_manager import file_manager as fm, MinimizationCase, MinimizationTestEnvironment
from pocketclustering.mutator import mutate_to_std_aminoacids
from pocketclustering.objects import TargetObject
from pocketclustering.pymol import pymol_reset, pymol_save, pymol_color_target_with_volume, mol_to_pymol, pymol_atomic_color
from pocketclustering.utils import execute_tasks, render_template

amber_bins = fm.get_folder_amber_bins()


class MinimizationInput:
    cluster: Cluster
    protein_pdb: str

    def __init__(self, cluster: Cluster, protein_pdb: str):
        self.cluster = cluster
        self.protein_pdb = protein_pdb


class MinimizationOutput:
    fgroup_pymol_name: str
    rmsd: float
    mol_min: Optional[Mol]
    mol_chem: Mol

    def __init__(self, fgroup_pymol_name: str, rmsd: float, mol_min: Optional[Mol], mol_chem: Mol):
        self.fgroup_pymol_name = fgroup_pymol_name
        self.rmsd = rmsd
        self.mol_min = mol_min
        self.mol_chem = mol_chem

    def is_good(self) -> bool:
        return self.mol_min is not None

    def get_pymol_color(self) -> str:
        if self.mol_min is None:
            return 'hotpink'
        elif self.rmsd < 0.5:
            return 'lime'
        elif self.rmsd < 1.0:
            return 'limegreen'
        elif self.rmsd < 1.5:
            return 'brightorange'
        else:
            return 'br8'


def _prepare_protein(pymol_inst: pymol2.PyMOL, amber_compatible_pdb: str) -> None:
    pymol_inst.cmd.delete('all')
    pymol_inst.cmd.load(amber_compatible_pdb)


def _prepare_fgroup(pymol_inst: pymol2.PyMOL, mol: Mol, output_pdb_with_h: str) -> None:
    fgroup_pymol_name = 'fgroup'

    mol_to_pymol(pymol_inst, mol, fgroup_pymol_name)
    pymol_inst.cmd.h_add(fgroup_pymol_name)
    pymol_inst.cmd.save(output_pdb_with_h, selection=fgroup_pymol_name)
    pymol_inst.cmd.delete(fgroup_pymol_name)


def minimize_task(minimization_input: MinimizationInput) -> MinimizationOutput:
    cluster = minimization_input.cluster
    protein_pdb = minimization_input.protein_pdb
    cluster_name = str(cluster)
    cluster_centroid_mol = cluster.get_centroid().mol
    print(f'Start minimization for {cluster}')

    mc = MinimizationCase(cluster_name)

    with pymol2.PyMOL() as pymol_inst:
        pymol_inst.cmd.set('retain_order', 1)
        _prepare_protein(pymol_inst, protein_pdb)
        _prepare_fgroup(pymol_inst, cluster_centroid_mol, mc.fg_pdb_with_h)
        minimize_data = minimize(pymol_inst, mc.fg_pdb_with_h, mc, 'MOL', cluster_name, cluster_centroid_mol)

    if minimize_data is not None:
        shutil.rmtree(mc.work_dir)
    else:
        minimize_data = MinimizationOutput(cluster_name, -1, None, cluster_centroid_mol)
        if not settings.KEEP_BAD_MINIMIZATION_CASES:
            shutil.rmtree(mc.work_dir)

    return minimize_data


def _num_resi(pymol_inst: pymol2.PyMOL) -> int:
    return len(set(atom.resi for atom in pymol_inst.cmd.get_model(f'polymer.protein').atom))


def minimize(pymol_inst: pymol2.PyMOL, file_fgroup_with_h: str, minimization_case: MinimizationCase, fgroup_resn: str, pymol_name: str, chem_mol: Mol) -> Optional[MinimizationOutput]:
    mc = minimization_case

    context = {
        'resn': fgroup_resn,
    }
    context.update(mc.as_dict())

    min_resi = 1
    max_resi = _num_resi(pymol_inst)

    render_template(fm.template_fgroup, mc.tleap_fgroup_in, context)
    render_template(fm.template_complex, mc.tleap_complex_in, context)
    render_template(fm.template_minimization, mc.minimization_in, minr=min_resi, maxr=max_resi)

    with open(mc.logfile, 'w') as logfile:
        # Assign Gasteiger charges to functional group and (important!) calculate net charge
        subprocess.run([f'{amber_bins}/antechamber', '-i', file_fgroup_with_h, '-fi', 'mdl', '-o', mc.fg_charged, '-fo', 'mol2', '-c', 'gas', '-at', 'sybyl', '-s', ' 2', '-j', '5', '-dr', 'no'],
                       cwd=mc.work_dir, stdout=logfile, stderr=logfile)
        # Transform Gasteiger charges to AM1-BCC charges
        subprocess.run([f'{amber_bins}/antechamber', '-i', mc.fg_charged, '-fi', 'mol2', '-o', mc.fg_mol2, '-fo', 'mol2', '-c', 'bcc', '-eq', '2', '-s', ' 2', '-j', '5', '-dr', 'no'],
                       cwd=mc.work_dir, stdout=logfile, stderr=logfile)
        # Create pdb for rmsd measurement
        subprocess.run([f'{amber_bins}/antechamber', '-i', mc.fg_mol2, '-fi', 'mol2', '-o', mc.fg_before, '-fo', 'mpdb', '-c', 'bcc', '-eq', '2', '-s', ' 2', '-j', '5', '-dr', 'no'],
                       cwd=mc.work_dir, stdout=logfile, stderr=logfile)

        try:
            pymol_inst.cmd.load(mc.fg_mol2, 'fgroup')
        except:
            return None
        pymol_inst.cmd.save(mc.complex_pdb)
        # Check params and generate .frcmod file
        subprocess.run([f'{amber_bins}/parmchk2', '-i', mc.fg_mol2, '-f', 'mol2', '-o', mc.fg_frcmod], cwd=mc.work_dir, stdout=logfile, stderr=logfile)

        # Preparing functional group and protein for minimization
        subprocess.run([f'{amber_bins}/tleap', '-f', mc.tleap_fgroup_in, '-s'], cwd=mc.work_dir, stdout=logfile, stderr=logfile)
        subprocess.run([f'{amber_bins}/tleap', '-f', mc.tleap_complex_in, '-s'], cwd=mc.work_dir, stdout=logfile, stderr=logfile)

        # Process minimization
        subprocess.run([f'{amber_bins}/sander', '-O', '-i', mc.minimization_in, '-o', mc.minimization_out, '-p', mc.cp_prmtop, '-c', mc.cp_rst7, '-r', mc.cp_ncrst], cwd=mc.work_dir, stdout=logfile,
                       stderr=logfile)

    # Generate output complex from minimization trajectory file
    with open(mc.final_pdb, 'w') as f:
        subprocess.run([f'{amber_bins}/ambpdb', '-p', mc.cp_prmtop, '-c', mc.cp_ncrst], stdout=f, cwd=mc.work_dir)

    # if minimization goes wrong and ambpdb write ******** instead of coordinates.
    # Pymol cant load this files
    with open(mc.final_pdb) as f:
        final_pdb_content = f.readlines()
    final_pdb_content = [x.strip() for x in final_pdb_content]
    for line in final_pdb_content:
        if '********' in line:
            return None

    result_in_pymol = 'result'
    pymol_inst.cmd.load(mc.final_pdb, result_in_pymol)

    pymol_inst.cmd.save(mc.fg_minimized, f'byres last {result_in_pymol}')
    mol_bef = Chem.MolFromPDBFile(mc.fg_before)
    mol_min = Chem.MolFromPDBFile(mc.fg_minimized)
    try:
        mol_min = AllChem.AssignBondOrdersFromTemplate(mol_bef, mol_min)
    except:
        return None

    if mol_bef.GetNumAtoms() != mol_min.GetNumAtoms():
        return None

    sym_map = list(zip(range(mol_bef.GetNumAtoms()), range(mol_min.GetNumAtoms())))
    rmsd = calc_rmsd(mol_bef, mol_min, sym_map)

    return MinimizationOutput(pymol_name, rmsd, mol_min, chem_mol)


# Assign chemical data from mol file to minimized functional group
def _mol_to_pymol_with_coords(pymol_inst: pymol2.PyMOL, mol_chem: Mol, mol_coord: Mol, name: str) -> None:
    tmp_pymol_name = f'{name}_with_coord'

    mol_to_pymol(pymol_inst, mol_coord, tmp_pymol_name)
    mol_to_pymol(pymol_inst, mol_chem, name)

    coord_space = {
        'coords': {}
    }
    pymol_inst.cmd.iterate_state(-1, tmp_pymol_name, 'coords[ID] = (x,y,z)', space=coord_space)
    pymol_inst.cmd.alter_state(-1, name, 'x,y,z = coords[ID]', space=coord_space)
    pymol_inst.cmd.delete(tmp_pymol_name)


def write_minimization_output(minimization_data: Iterable[MinimizationOutput], target_obj: TargetObject, output_name: str) -> None:
    rmsd_data = {}
    with pymol2.PyMOL() as pymol_inst:
        pymol_inst.cmd.set('group_auto_mode', 2)
        pymol_reset(pymol_inst, target_obj.path_to_pse)
        pymol_color_target_with_volume(pymol_inst, str(target_obj), settings.VOLUME_OBJ)
        for md in minimization_data:
            if md.is_good():
                pymol_name = f'{md.fgroup_pymol_name}.std'
                pymol_name_minimized = f'{md.fgroup_pymol_name}.min'
                rmsd_data[md.fgroup_pymol_name] = md.rmsd
                mol_to_pymol(pymol_inst, md.mol_chem, pymol_name)
                _mol_to_pymol_with_coords(pymol_inst, md.mol_chem, md.mol_min, pymol_name_minimized)
                pymol_inst.cmd.disable(pymol_name_minimized)
            else:
                pymol_name = f'{md.fgroup_pymol_name}_err.std'
                mol_to_pymol(pymol_inst, md.mol_chem, pymol_name)

            pymol_atomic_color(pymol_inst, pymol_name, md.get_pymol_color())

        pymol_save(pymol_inst, fm.get_output_file(output_name, '_min.pse'))

    with open(fm.get_output_file(output_name, '_rmsd.txt'), 'w') as f:
        f.write('Name\trmsd\n')
        for fgroup_pymol_name, rmsd in rmsd_data.items():
            f.write(f'{fgroup_pymol_name}\t{rmsd}\n')
            print(f'{fgroup_pymol_name}\tRMSD: {rmsd}')


# Check AMBER bins exist
def check_amber_binaries() -> None:
    amb_bins = ['antechamber', 'parmchk2', 'tleap', 'sander', 'ambpdb']
    amber_paths = [settings.AMBER_FOLDER.joinpath('bin').joinpath(amb_bin) for amb_bin in amb_bins]
    bad_paths = []
    for amber_path in amber_paths:
        if not os.path.exists(amber_path):
            bad_paths.append(amber_path)
    if len(bad_paths) != 0:
        print('AMBER bins not found:')
        for bad_path in bad_paths:
            print(str(bad_path))
        print('Check AMBER installation and make sure that the settings.AMBER_FOLDER is set correctly')
        raise FileNotFoundError('AMBER not found')


# Check Protein parametrization, return mutated pdb if necessary.
def prepare_pdb_for_amber(target_obj: TargetObject) -> str:
    with MinimizationTestEnvironment(str(target_obj)) as mte:
        with pymol2.PyMOL() as pymol_inst:
            pymol_reset(pymol_inst, target_obj.path_to_pse)
            pymol_inst.cmd.delete(settings.VOLUME_OBJ)
            amber_compatible_pdb = fm.create_temp_file('.pdb')
            pymol_inst.cmd.save(amber_compatible_pdb)

            pymol_inst.cmd.set('retain_order', 1)
            _prepare_protein(pymol_inst, amber_compatible_pdb)
            pymol_inst.cmd.save(mte.pdb_for_test)

        r = subprocess.run([settings.AMBER_FOLDER.joinpath('bin').joinpath('tleap'), '-f', mte.tleap_complex_in, '-s'], cwd=mte.work_dir, capture_output=True)
        if r.returncode != 0:
            print('The target pdb cannot be parameterized for minimization. Trying to mutate not standard amino acids.')
            amber_compatible_pdb = fm.create_temp_file('_mutated.pdb')
            mutate_to_std_aminoacids(mte.pdb_for_test, amber_compatible_pdb)

            # Try to parametrize mutated pdb
            shutil.copyfile(amber_compatible_pdb, mte.pdb_for_test)
            r = subprocess.run([settings.AMBER_FOLDER.joinpath('bin').joinpath('tleap'), '-f', mte.tleap_complex_in, '-s'], cwd=mte.work_dir, capture_output=True)
            if r.returncode == 0:
                print('Protein successfully mutated for minimization!')
            else:
                os.unlink(amber_compatible_pdb)
                raise Exception('The mutation did not help to parameterize the target protein, try to mutate protein by self.')

    return amber_compatible_pdb


def minimize_clusters(clusters: Iterable[Cluster], target_obj: TargetObject, output_name: str, amber_compatible_pdb: str):
    minimization_input = []
    for cluster in clusters:
        minimization_input.append(MinimizationInput(cluster, amber_compatible_pdb))

    minimization_output = execute_tasks(minimization_input, settings.IS_MP_MINIMIZATION, minimize_task, None)
    write_minimization_output(minimization_output, target_obj, output_name)
