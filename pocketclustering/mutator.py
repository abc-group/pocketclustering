import pymol
from Bio.Data.IUPACData import protein_letters_1to3
from Bio.Data.SCOPData import protein_letters_3to1


def mutate_to_std_aminoacids(file_in: str, file_out: str) -> None:
    pymol.finish_launching(['pymol', '-qc'])  # Important, because mutation wizard don’t work in pymol2 instances.

    obj_name = 'obj_name'
    pymol.cmd.load(file_in, obj_name)

    obj_sele = f'{obj_name} and polymer.protein'
    space = {'residues': []}
    pymol.cmd.iterate(obj_sele, 'residues.append((resi, resn))', space=space)
    residues = set(space['residues'])

    residues = list(filter(lambda x: x[1].title() not in protein_letters_1to3.values(), residues))
    mutations = [(res[0], protein_letters_1to3[protein_letters_3to1[res[1]]].upper()) for res in residues]

    for residue, mutation in zip(residues, mutations):
        print(f'Mutation: {residue} -> {mutation}')

    pymol.cmd.wizard("mutagenesis")
    for mutation in mutations:
        pymol.cmd.get_wizard().do_select(f'resi {mutation[0]}')
        pymol.cmd.get_wizard().set_mode(mutation[1])
        pymol.cmd.get_wizard().apply()
    pymol.cmd.wizard(None)
    pymol.cmd.save(file_out)
    pymol.cmd.quit()
