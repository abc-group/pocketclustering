import csv
import os
from typing import List, Tuple, DefaultDict

import json
import pymol2
from rdkit import Chem
from rdkit.Chem.rdchem import Mol

from pocketclustering.utils import escape_chars, smiles_escape_chars
from pocketclustering.file_manager import file_manager as fm


class FunctionalGroup:
    mol: Mol
    smiles: str
    smiles_escaped: str
    name: str
    full_name: str

    def __init__(self, pymol_inst: pymol2.PyMOL, selection: str, name: str):
        tmp_mol_file = fm.create_temp_file('.mol')
        pymol_inst.cmd.save(tmp_mol_file, selection=selection)
        self.mol = Chem.MolFromMolFile(tmp_mol_file)
        os.unlink(tmp_mol_file)

        self.smiles = Chem.MolToSmiles(self.mol)
        self.smiles_escaped = smiles_escape_chars(self.smiles)
        self.name = name
        self.full_name = name + ' ' + self.smiles


class FunctionalGroupMatcher:
    name: str
    smarts_mol: Mol

    def __init__(self, name: str, smarts: str):
        self.name = name
        self.smarts_mol = Chem.MolFromSmarts(smarts)
        if self.smarts_mol is None:
            raise ValueError(f'Wrong SMARTS: {smarts}')

    def has_match(self, mol: Mol) -> bool:
        return mol.HasSubstructMatch(self.smarts_mol)

    def get_matches(self, mol: Mol, uniquify: bool = True) -> Tuple[Tuple[int, ...], ...]:
        return mol.GetSubstructMatches(self.smarts_mol, uniquify=uniquify)


def init_functional_groups(smarts_file: str, force_uniq: bool) -> List[FunctionalGroupMatcher]:
    fgroups_list = []

    with open(smarts_file) as tsvfile:
        reader = csv.reader(tsvfile, delimiter='\t')
        i = 1
        for row in reader:
            if row[0][0] != '#':
                name, smart = row[0], row[1]
                if force_uniq:
                    name = '%03d_%s' % (i, name)
                fgroups_list.append(FunctionalGroupMatcher(escape_chars(name), smart))
                i += 1

    return fgroups_list


def write_functional_groups_info(fgroups_info_data: DefaultDict[str, List[str]], filename_prefix: str) -> None:
    with open(fm.get_output_file(filename_prefix, '_fg.txt'), 'w') as fp:
        json.dump(fgroups_info_data, fp, sort_keys=True, indent=2)
