from enum import Enum
from typing import Optional, Dict, Callable

import pymol2
from rdkit import Chem
from rdkit.Chem import rdchem, Mol

import settings


class SanitizeError(Exception):
    pass


class LocationMark(Enum):
    not_symmetry = 1
    self_chain = 2
    other_chain = 3
    symmetry = 4


class ResidueLocation:
    similar_obj_id: str
    obj_name: str
    chain: str
    resn: str
    resi: str
    is_ligand: bool
    location_mark: LocationMark

    def __init__(self, similar_obj_id: str, obj_name: str, chain: str, resn: str, resi: str, is_ligand: bool, location_mark: LocationMark):
        self.similar_obj_id = similar_obj_id
        self.obj_name = obj_name
        self.chain = chain
        self.resn = resn
        self.resi = resi
        self.is_ligand = is_ligand
        self.location_mark = location_mark

    def to_json(self) -> str:
        m = 'L' if self.is_ligand else 'P'
        return f'{self.similar_obj_id} | {self.obj_name} {self.chain} {self.resn} {self.resi} {m} {self.location_mark.name}'

    def __str__(self):
        return f'{self.similar_obj_id}_{self.resn}_{self.resi}'


class ResidueObject:
    is_ligand: bool
    location: ResidueLocation

    def __init__(self, pymol_inst: pymol2.PyMOL, similar_obj_id: str, obj_name: str, chain: str, resn: str, resi: str, location_mark: LocationMark):

        self.is_ligand = self.verify_is_ligand(pymol_inst, obj_name, resi)
        self.location = ResidueLocation(similar_obj_id, obj_name, chain, resn, resi, self.is_ligand, location_mark)

    def __str__(self):
        return f'{self.location.obj_name}_{self.location.chain}_{self.location.resi}_{self.location.resn}_{self.location.location_mark}'

    @staticmethod
    def verify_is_ligand(pymol_inst, obj_name, resi):
        sel_lig = f'{obj_name} and resi {resi} and organic'
        num_atoms_is_lig = pymol_inst.cmd.select('sel_is_lig', selection=sel_lig)
        is_ligand = (num_atoms_is_lig != 0)
        return is_ligand

    @staticmethod
    def refine_location(location, res_chain, sim_obj):
        if location == LocationMark.not_symmetry:
            if res_chain == sim_obj.chain:
                return LocationMark.self_chain
            else:
                return LocationMark.other_chain
        else:
            return location


def assign_bond_orders_from_template(ref_mol: Mol, mol: Mol, force_assignment: bool = False) -> Mol:
    """
    Improved version of AssignBondOrdersFromTemplate from RdKit
    """
    refmol2 = rdchem.Mol(ref_mol)
    mol2 = rdchem.Mol(mol)
    # do the molecules match already?
    matching = mol2.GetSubstructMatch(refmol2)
    if not matching or force_assignment:  # no, they don't match
        # check if bonds of mol are SINGLE
        # noinspection PyArgumentList
        for b in mol2.GetBonds():
            if b.GetBondType() != Chem.BondType.SINGLE:
                b.SetBondType(Chem.BondType.SINGLE)
                b.SetIsAromatic(False)
        # set the bonds of mol to SINGLE
        # noinspection PyArgumentList
        for b in refmol2.GetBonds():
            b.SetBondType(Chem.BondType.SINGLE)
            b.SetIsAromatic(False)
        # set atom charges to zero;
        # noinspection PyArgumentList
        for a in refmol2.GetAtoms():
            a.SetFormalCharge(0)
        # noinspection PyArgumentList
        for a in mol2.GetAtoms():
            a.SetFormalCharge(0)

        matching = mol2.GetSubstructMatches(refmol2, uniquify=False)
        # do the molecules match now?
        if matching:
            matching = matching[0]
            # apply matching: set bond properties
            for b in ref_mol.GetBonds():
                atom1 = matching[b.GetBeginAtomIdx()]
                atom2 = matching[b.GetEndAtomIdx()]
                b2 = mol2.GetBondBetweenAtoms(atom1, atom2)
                b2.SetBondType(b.GetBondType())
                b2.SetIsAromatic(b.GetIsAromatic())
            # apply matching: set atom properties
            for a in ref_mol.GetAtoms():
                a2 = mol2.GetAtomWithIdx(matching[a.GetIdx()])
                a2.SetHybridization(a.GetHybridization())
                a2.SetIsAromatic(a.GetIsAromatic())
                a2.SetNumExplicitHs(a.GetNumExplicitHs())
                a2.SetFormalCharge(a.GetFormalCharge())
            Chem.SanitizeMol(mol2)
            if hasattr(mol2, '__sssAtoms'):
                mol2.__sssAtoms = None  # we don't want all bonds highlighted
        else:
            raise ValueError("No matching found")
    return mol2


def load_residue_from_mol(res_mol_file: str, smiles: Optional[str] = None, resn: Optional[str] = None, try_default_sanitization: bool = False):
    def _load_mol_with_ref(mol_file: str, ref_mol: Mol) -> Mol:
        if ref_mol is None:
            raise ValueError('Cant create ref mol')
        mol = Chem.MolFromMolFile(str(mol_file), sanitize=False)
        if mol is None:
            raise ValueError('Cant load mol from file')
        mol = assign_bond_orders_from_template(ref_mol, mol, force_assignment=True)
        if mol is None:
            raise ValueError('Sanitization Error')
        return mol

    def from_catalog(residue_name: str, mol_file: str, catalog: Dict, ref_mol_func: Callable) -> Mol:
        if residue_name not in catalog:
            raise ValueError(f'No {residue_name} in catalog')
        data = catalog[residue_name]
        ref_mol = ref_mol_func(data)
        mol = _load_mol_with_ref(mol_file, ref_mol)

        return mol

    def from_smiles(mol_file: str, reference_smiles: str) -> Mol:
        ref_mol = Chem.MolFromSmiles(reference_smiles)
        mol = _load_mol_with_ref(mol_file, ref_mol)
        return mol

    if try_default_sanitization:
        res_mol = Chem.MolFromMolFile(str(res_mol_file))
    else:
        res_mol = None

    if (res_mol is None) and (smiles is not None):
        try:
            res_mol = from_smiles(res_mol_file, smiles)
        except ValueError as e:
            raise SanitizeError(f'SanitizeError: {e}')
    elif (res_mol is None) and (resn is not None):
        try:
            res_mol = from_catalog(resn, res_mol_file, settings.CAT_SMILES_OE, Chem.MolFromSmiles)
        except ValueError:
            try:
                res_mol = from_catalog(resn, res_mol_file, settings.CAT_SMILES_CACTVS, Chem.MolFromSmiles)
            except ValueError:
                try:
                    res_mol = from_catalog(resn, res_mol_file, settings.CAT_INCHI, Chem.MolFromInchi)
                except ValueError as e:
                    raise SanitizeError(f'SanitizeError({resn}): {e}')

    return res_mol
