import os
import unittest
from unittest.mock import patch

import pymol2

import pocketclustering.utils as pk_u
import pocketclustering.file_manager as pk_fm

data_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'data')

PATH_CAT_TST = os.path.join(data_dir, 'cat_tst.txt')


class TestUtils(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    # Tests:
    @patch('settings.FIND_IN_PROTEIN', False)
    @patch('settings.FIND_IN_LIGAND', False)
    @patch('settings.FIND_ONLY_IN_SELF_CHAIN', True)
    @patch('settings.NOT_FIND_IN_SELF_CHAIN', True)
    def test_full_suff(self):
        suff = pk_fm.make_output_suffix('smarts')
        self.assertEqual(suff, 'smarts_np_nl_selfchain_notselfchain')

    @patch('settings.FIND_IN_PROTEIN', True)
    @patch('settings.FIND_IN_LIGAND', True)
    @patch('settings.FIND_ONLY_IN_SELF_CHAIN', False)
    @patch('settings.NOT_FIND_IN_SELF_CHAIN', False)
    def test_short_suff(self):
        suff = pk_fm.make_output_suffix('smarts')
        self.assertEqual(suff, 'smarts')

    def test_read_catalog(self):
        cat = pk_u.read_catalog(PATH_CAT_TST)
        self.assertIn('000', cat)
        self.assertIn('1X2', cat)
        self.assertIn('1X3', cat)
        self.assertNotIn('PPP', cat)
        self.assertEqual(cat['1X2'], 'c1ccc(cc1)CCCCCCN=C=S')
        self.assertEqual(len(cat), 7)

    def test_escape_chars(self):
        new_string = pk_u.escape_chars('sad.[ _s_\\]')
        self.assertEqual(new_string, 'sad.___s___')

    def test_smiles_escape_chars(self):
        new_string = pk_u.smiles_escape_chars('[с]с\\\t\ncc')
        self.assertEqual(new_string, '_с_с_tncc')
