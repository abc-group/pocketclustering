import os
from types import SimpleNamespace
from typing import List, Optional, Iterable

import pymol2
from rdkit import Chem
from rdkit.Chem.rdchem import Mol

from pocketclustering.file_manager import file_manager as fm


class ArgumentsError(Exception):
    pass


def mol_to_pymol(pymol_inst: pymol2.PyMOL, mol: Mol, pymol_name: str, conf_id: int = -1) -> None:
    tmp_mol_file = fm.create_temp_file('.mol')
    Chem.MolToMolFile(mol, tmp_mol_file, confId=conf_id)
    pymol_inst.cmd.load(tmp_mol_file, pymol_name)
    os.unlink(tmp_mol_file)


def pymol_extract_atom_properties(pymol_instance: pymol2.PyMOL, variables: Iterable[str], selection: str = '(all)') -> List[SimpleNamespace]:
    space = {'objects': []}
    variables_string = ', '.join(variables)

    pymol_instance.cmd.iterate(selection, f'objects.append(({variables_string}, ))', space=space)
    objects = set(space['objects'])

    result = []
    for obj in objects:
        _dict = {}
        for atom_property, value in zip(variables, obj):
            _dict[atom_property] = value
        result.append(SimpleNamespace(**_dict))

    return result


def load_object(pymol_inst: pymol2.PyMOL, object_name: str, chain: Optional[str] = None, pdb_id: Optional[str] = None, path: Optional[str] = None) -> str:
    folder_cif = fm.get_folder_cif()
    if pdb_id is None and path is None:
        raise FileNotFoundError('pdb_id or path must be specified.')
    if path is None:
        path = os.path.join(folder_cif, f'{pdb_id.lower()}.cif')
        cur_pwd = os.getcwd()
        if not os.path.exists(path):
            print(f'Downloading file for {pdb_id}')
        cur_cm = pymol_inst.cmd.get('connect_mode')
        pymol_inst.cmd.cd(folder_cif)
        pymol_inst.cmd.set('connect_mode', 4)
        pymol_inst.cmd.fetch(pdb_id, str(object_name), async_=0)
        pymol_inst.cmd.cd(cur_pwd)
        pymol_inst.cmd.set('connect_mode', cur_cm)
    else:
        if not os.path.exists(path):
            raise ArgumentsError(f'File: {path} not exist.')
        pymol_inst.cmd.load(path, object=object_name)

    if chain is not None:
        pymol_inst.cmd.remove(f'{object_name} and (not chain {chain})')
    pymol_inst.cmd.remove(f'{object_name} and inorganic')
    pymol_inst.cmd.remove(f'{object_name} and solvent')
    pymol_inst.cmd.remove(f'{object_name} and resn DOD')
    pymol_inst.cmd.remove(f'{object_name} and hydrogens')

    return path


def pymol_remove_far(pymol_inst: pymol2.PyMOL, target: str, volume_obj: str, volume_radius: float, extend_by: str = 'byres') -> None:
    pymol_inst.cmd.remove(f' (({target}) and not ({extend_by} (({target}) within {volume_radius} of {volume_obj})))')


def pymol_reset(pymol_inst: pymol2.PyMOL, base_pse: Optional[str] = None):
    pymol_inst.cmd.delete('all')
    if base_pse is not None:
        pymol_inst.cmd.load(base_pse)


def pymol_color_target_with_volume(pymol_inst: pymol2.PyMOL, target_obj: str, volume_obj: str) -> None:
    target_obj = str(target_obj)
    pymol_inst.cmd.color('lightteal', selection=target_obj)
    pymol_inst.cmd.hide(selection=volume_obj)
    pymol_inst.cmd.show('dots', volume_obj)
    pymol_inst.cmd.hide(selection=target_obj)
    pymol_inst.cmd.show('surface', target_obj)
    pymol_inst.cmd.color('cyan', volume_obj)
    pymol_inst.cmd.color('lightblue', f'{target_obj} and (resn arg+lys+his)')  # positive
    pymol_inst.cmd.color('lightpink', f'{target_obj} and (resn glu+asp)')  # negative
    pymol_inst.cmd.enable(target_obj)
    pymol_inst.cmd.enable(volume_obj)


def pymol_save(pymol_inst: pymol2.PyMOL, file_name: str, orient: Optional[str] = None) -> None:
    if orient is None:
        pymol_inst.cmd.orient()
    else:
        pymol_inst.cmd.orient(orient)
    pymol_inst.cmd.deselect()
    pymol_inst.cmd.save(file_name)


def pymol_atomic_color(pymol_inst: pymol2.PyMOL, selection: str, color: str):
    pymol_inst.cmd.color(color, selection)
    pymol_inst.cmd.color('atomic', f'({selection}) and (not elem C)')
