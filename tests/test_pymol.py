import os
import unittest
from unittest.mock import patch

import pymol2

from pocketclustering.objects import TargetObject
import pocketclustering.pymol as pk_pm

data_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'data')

PATH_3FPO = os.path.join(data_dir, '3fpo.cif')
PATH_3FPO_AROM6 = os.path.join(data_dir, '3fpo_arom6.pdb')

VOLUME_OBJ = 'Vol'


class TestPymol(unittest.TestCase):
    @patch('settings.VOLUME_OBJ', VOLUME_OBJ)
    def setUp(self):
        self.pymol_instance = pymol2.PyMOL()

        self.target_obj = TargetObject(PATH_3FPO, 'A', PATH_3FPO_AROM6)

        self.pymol_instance.start()

    def tearDown(self):
        self.pymol_instance.stop()

    def get_objects_list(self, selection='(all)'):
        return self.pymol_instance.cmd.get_object_list(selection)

    # Tests:

    def test_pymol_load(self):
        obj_nm = 'tst_obj'
        objs = self.get_objects_list()
        self.assertEqual(len(objs), 0)
        pk_pm.load_object(self.pymol_instance, obj_nm, path=PATH_3FPO)
        objs = self.get_objects_list()
        self.assertEqual(len(objs), 1)
        self.assertIn(obj_nm, objs)

    def test_pymol_remove_far(self):
        pk_pm.pymol_reset(self.pymol_instance, self.target_obj.path_to_pse)
        c1 = self.pymol_instance.cmd.select('all')
        pk_pm.pymol_remove_far(self.pymol_instance, self.target_obj, VOLUME_OBJ, 16)
        c2 = self.pymol_instance.cmd.select('all')
        self.assertEqual(c1, c2)

        c1 = self.pymol_instance.cmd.select('all')
        pk_pm.pymol_remove_far(self.pymol_instance, self.target_obj, VOLUME_OBJ, 1, extend_by='bychain')
        c2 = self.pymol_instance.cmd.select('all')
        self.assertEqual(c1, c2)

        pk_pm.pymol_remove_far(self.pymol_instance, self.target_obj, VOLUME_OBJ, 0.5)
        c2 = self.pymol_instance.cmd.select(f'all and {self.target_obj}')
        self.assertEqual(c2, 12)  # 12 atoms in residue which near volume object

    def test_pymol_reset(self):
        pk_pm.pymol_reset(self.pymol_instance)
        objs = self.get_objects_list()
        self.assertEqual(len(objs), 0)
        pk_pm.pymol_reset(self.pymol_instance, self.target_obj.path_to_pse)
        objs = self.get_objects_list()
        self.assertEqual(len(objs), 2)
        self.assertIn(VOLUME_OBJ, objs)
        self.assertIn(str(self.target_obj), objs)
        objs = self.get_objects_list()
        self.assertEqual(len(objs), 2)
        self.assertIn(VOLUME_OBJ, objs)
        self.assertIn(str(self.target_obj), objs)
