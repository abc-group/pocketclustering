import csv
import multiprocessing
from typing import List, Optional, Any, Callable, Dict, Tuple, Iterable

import numpy as np
from jinja2 import Template
from rdkit import Chem
from rdkit.Chem.rdchem import Mol, Conformer

import settings


def gen_sym_maps(mols: List[Mol]):
    for i in range(len(mols)):
        ranks = list(Chem.CanonicalRankAtoms(mols[i]))
        reordering = [x for (x, y) in sorted(list(enumerate(ranks)), key=lambda x: x[1])]
        mols[i] = Chem.RenumberAtoms(mols[i], reordering)

    # Find all symmetry mappings
    sym_maps = [list(enumerate(self_match)) for self_match in mols[0].GetSubstructMatches(mols[0], uniquify=False, useChirality=True)]
    return sym_maps


def calc_rmsd(mol1: Mol, mol2: Mol, sym_maps: Optional[List[Tuple[Any, Any]]] = None, mol1_conformer: Optional[Conformer] = None, mol2_conformer: Optional[Conformer] = None):
    if mol1_conformer is None:
        mol1_coords = mol1.GetConformer().GetPositions()
    else:
        mol1_coords = mol1_conformer.GetPositions()

    if mol2_conformer is None:
        mol2_coords = mol2.GetConformer().GetPositions()
    else:
        mol2_coords = mol2_conformer.GetPositions()
    sd = [np.sum((mol1_coords[i] - mol2_coords[j]) ** 2) for i, j in sym_maps]
    rmsd = np.sqrt(np.mean(sd))
    return rmsd


def render_template(filename_in: str, filename_out: str, *args, **kwargs):
    template = Template(open(filename_in).read())
    with open(filename_out, 'w') as fp:
        fp.write(template.render(*args, **kwargs))


def execute_tasks(inputs: Iterable[Any], is_multiprocessing: bool, processing_function: Callable, parser_function: Optional[Callable] = None) -> Any:
    if settings.NCPU is None:
        ncpu = multiprocessing.cpu_count() - 1
    else:
        ncpu = settings.NCPU
    if is_multiprocessing:
        with multiprocessing.Pool(ncpu) as pool:
            results = pool.map(processing_function, inputs)
    else:
        results = [processing_function(task) for task in inputs]
    print(f'Task {processing_function.__name__} done.')
    if parser_function is not None:
        results = parser_function(results)
    return results


def read_catalog(catalog_file: str) -> Dict:
    catalog = {}

    with open(catalog_file) as tsvfile:
        reader = csv.reader(tsvfile, delimiter='\t')
        for row in reader:
            if len(row) >= 2:
                name, data = row[1], row[0]
                catalog[name] = data
    return catalog


def escape_chars(s: str) -> str:
    safe_char = '_'
    bad_chars = ' ()=+-,*/\\#@[]'
    for bad_char in bad_chars:
        s = s.replace(bad_char, safe_char)
    return s


def smiles_escape_chars(smiles_str: str) -> str:
    dict_replace = {
        '=': '_double_',
        '[': '_',
        ']': '_',
        '/': '_',
        '\\': '_',
        '\t': 't',
        '\n': 'n'
    }
    for bad_char, good_char in dict_replace.items():
        smiles_str = good_char.join(smiles_str.split(bad_char))

    return smiles_str
