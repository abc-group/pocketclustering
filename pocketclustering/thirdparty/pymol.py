import pymol2


class NothingToAlignException(Exception):
    pass


def pymol_focus_alignment(pymol_inst: pymol2.PyMOL, obj1: str, obj2: str, sel: str, debug: bool = False) -> float:
    # Author: Jason Vertrees
    # Date  : 08-17-2011
    """
    Source: https://pymolwiki.org/index.php/Focus_alignment

    Attention:
    1)
    typo in original string 'elif sel_model==modelB[0]:'
    right string is 'elif ssel_model == modelB[0]:'
    2)
    In this moment cmd.pair_fit have critical bug which dont allow use this function twice in a row in different pymol instances
    Example in: _pair_fit_bug.py
    3)
    Added check for selections in alignment
    4)
    Added type annotations

    Instead of cmd.pair_fit, cmd.align is used
    """

    '''
    PARAMS
      obj1
          structure 1
      obj2
          structure 2
      sel
          the selection from either obj1 or
          obj2 to focus the pair_fitting on.
          When providing this selection, please
          make sure you also specify selected
          atoms from ONE object.
    NOTES
    This function will first align obj1 and obj2 using
    a sequence alignment. This creates a mapping of
    residues from obj1 to obj2. Next, the selection, sel,
    is used to find only those atoms in the alignment and
    in sel. These atoms are paired with their mapped
    atoms from the alignment in the other object. These
    two subsets of atoms are then pair_fit to give
    an optimal sub-alignment.
    '''

    obj1 = str(obj1)
    obj2 = str(obj2)

    if pymol_inst.cmd.select(sel) == 0:
        raise NothingToAlignException('sel string is Empty')

    aln = 'aln'
    _sel = '__sel'
    ssel_model = ''
    a1, a2, a_target, modelA, modelB, sel_model = [], [], [], [], [], []
    obj1, obj2 = 'poly and ' + obj1, 'poly and ' + obj2

    # space dictionary for iterate
    space = {'a1': a1,
             'a2': a2,
             'a_target': a_target,
             'sel_model': sel_model,
             'modelA': modelA,
             'modelB': modelB}

    if pymol_inst.cmd.select(obj1) == 0:
        raise NothingToAlignException(f'{obj1} is Empty')
    # initial unfocused alignment
    pymol_inst.cmd.align(obj1, obj2, cycles=0, object=aln)

    # record the initial indices
    s = 'n. CA and (%s and %s)'
    pymol_inst.cmd.iterate(s % (obj1, aln), 'a1.append(index)', space=space)
    pymol_inst.cmd.iterate(s % (obj2, aln), 'a2.append(index)', space=space)

    if debug:
        print('# [debug] num atom in aln1 = ', len(a1))
        print('# [debug] num atom in aln2 = ', len(a2))

    # determine who owns the focused selection and
    # get canonical object names
    pymol_inst.cmd.iterate('first %s' % sel, 'sel_model.append(model)', space=space)
    pymol_inst.cmd.iterate('first %s' % obj1, 'modelA.append(model)', space=space)
    pymol_inst.cmd.iterate('first %s' % obj2, 'modelB.append(model)', space=space)
    ssel_model = sel_model[0]

    if debug:
        print('# [debug] selection is in object %s' % ssel_model)

    # focus the target selection
    pymol_inst.cmd.iterate(sel + ' and n. CA', 'a_target.append(index)', space=space)

    if debug:
        print('# [debug] a_target has %d members.' % len(a_target))

    # select the correct object from which to index

    target_list = None
    if ssel_model == modelA[0]:
        target_list = a1
    elif ssel_model == modelB[0]:
        target_list = a2
    else:
        print('# error: selection on which to focus was not found')
        print('# error: in either object passed in.')
        print(sel_model)
        print(modelA)
        print(modelB)
        return -1

    id1, id2 = [], []
    for x in a_target:
        try:
            idx = target_list.index(x)
            if debug:
                print('Current index: %d' % idx)
            id1.append(str(a1[idx]))
            id2.append(str(a2[idx]))
        except:
            pass

    if debug:
        print('# [debug] id1 = %s' % id1)
        print('# [debug] id2 = %s' % id2)

    if len(id1) == 0:
        raise NothingToAlignException('No structures matches')

    sel1 = '+'.join(id1)
    sel2 = '+'.join(id2)

    if debug:
        print('# [debug] sel1 = %s' % sel1)

    # pm.cmd.pair_fit(obj1 + " and aln and index " + sel1, obj2 + " and aln and index " + sel2)
    rmsd = pymol_inst.cmd.align(obj1 + ' and aln and index ' + sel1, obj2 + ' and aln and index ' + sel2, cycles=0)[0]

    return rmsd
