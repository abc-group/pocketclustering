import os
from typing import Optional, List, Dict, Set

import pymol2
from Bio.Data.SCOPData import protein_letters_3to1
from Bio.PDB.MMCIF2Dict import MMCIF2Dict

import settings
from pocketclustering.file_manager import file_manager as fm
from pocketclustering.pymol import load_object, pymol_remove_far, pymol_extract_atom_properties
from pocketclustering.residues import LocationMark, ResidueObject
from pocketclustering.utils import escape_chars


class ProteinObject:
    pdb_id: str
    custom_file_path: Optional[str]
    chain: str
    _path_cif: Optional[str]
    _resolution: Optional[float]
    _structure_method: Optional[str]

    def __init__(self, pdb_id: str, chain: str, is_custom_file: bool = False):
        if is_custom_file:
            self.pdb_id = escape_chars(fm.name_from_patch(pdb_id)).upper()
            self.custom_file_path = pdb_id
        else:
            self.pdb_id = pdb_id.upper()
            self.custom_file_path = None
        self.chain = chain
        self._path_cif = None
        self._resolution = None
        self._structure_method = None

    def get_full_name(self) -> str:
        return f'{self.pdb_id}{self.chain}'

    def get_path_cif(self) -> str:
        return self._path_cif

    def set_path_cif(self, path_cif) -> None:
        self._path_cif = path_cif

    def get_resolution(self) -> float:
        return self._resolution

    def get_structure_method(self) -> str:
        return self._structure_method

    def assign_header_info(self, method: str, resolution: float) -> None:
        self._resolution = float(resolution)
        self._structure_method = method

    def load_header_info(self) -> None:
        if self._path_cif is None:
            raise Warning("Path cif is None")
        struct = MMCIF2Dict(self._path_cif)
        entry_resolution = '_refine.ls_d_res_high'
        entry_structure_method = '_exptl.method'

        if entry_resolution in struct:
            resolution = struct[entry_resolution]
            try:
                if isinstance(resolution, list):
                    self._resolution = min(float(r) for r in resolution)
                else:
                    self._resolution = float(resolution)
            except ValueError:
                self._resolution = None

        if entry_structure_method in struct:
            method = struct[entry_structure_method]
            if not isinstance(method, str):
                if 'X-RAY DIFFRACTION' in method:
                    self._structure_method = 'X-RAY DIFFRACTION'
                else:
                    self._structure_method = method[0]
            else:
                self._structure_method = method

    def __str__(self):
        return self.get_full_name()

    def get_pymol_name(self) -> str:
        return str(self)

    def __lt__(self, other):
        return str(self) < str(other)


class TargetObject(ProteinObject):
    path_to_pse: str
    volume: str
    fasta: str
    ind_to_resi: List[str]
    resi_to_ind: Dict[str, int]
    resi_in_pocket: Set[str]

    def _init_target(self, pymol_inst, path_to_volume):
        pymol_inst.cmd.load(path_to_volume, object={settings.VOLUME_OBJ})
        load_object(pymol_inst, self.get_pymol_name(), chain=self.chain, pdb_id=self.pdb_id, path=self.custom_file_path)
        pymol_inst.cmd.remove(f'{self} and (not polymer)')
        pymol_inst.cmd.remove(f'{self} and (not present)')
        tmp_pse_file = fm.create_temp_file('.pse')
        pymol_inst.cmd.save(tmp_pse_file)
        if settings.DEBUG:
            print(f"Target_obj: {tmp_pse_file}")
        self.path_to_pse = tmp_pse_file

    def __init__(self, pdbid_or_file, chain, path_to_volume):
        if os.path.isfile(pdbid_or_file):
            print(f'{pdbid_or_file} file detected')
            super().__init__(pdbid_or_file, chain, is_custom_file=True)
        else:
            if len(pdbid_or_file) != 4:
                raise Warning(f'File with name {pdbid_or_file} not found or PDB ID is incorrect')
            super().__init__(pdbid_or_file, chain)
        self.volume = path_to_volume
        with pymol2.PyMOL() as pymol_inst:
            pymol_inst.cmd.set('retain_order', 1)
            self._init_target(pymol_inst, path_to_volume)
            residues_ranked = pymol_extract_atom_properties(pymol_inst, ['resi', 'resn', 'rank', 'chain', 'model'], selection=f'{self.get_pymol_name()} and polymer.protein')
            residues_ranked = sorted(residues_ranked, key=lambda x: x.rank)
            # Check that passed one chain from one object
            model_chain = (residues_ranked[0].model, residues_ranked[0].chain)
            for case in residues_ranked:
                if case.model != model_chain[0] or case.chain != model_chain[1]:
                    raise Exception('Trying to get fasta string from different objects or chains')

            self.fasta = ''
            self.ind_to_resi = []
            for res_obj in residues_ranked:
                if res_obj.resi not in self.ind_to_resi:
                    self.fasta += protein_letters_3to1[res_obj.resn]
                    self.ind_to_resi.append(res_obj.resi)

            self.resi_to_ind = {}
            for ind, resi in enumerate(self.ind_to_resi):
                self.resi_to_ind[resi] = ind

            self.resi_in_pocket = set()
            for atom in pymol_inst.cmd.get_model(f'{self} within {settings.POCKET_ALIGNMENT_RADIUS} of {settings.VOLUME_OBJ}').atom:
                self.resi_in_pocket.add(atom.resi)

    def __str__(self):
        return f'target_{self.get_full_name()}'

    def finish(self) -> None:
        os.unlink(self.path_to_pse)


class SimilarObject(ProteinObject):
    _list_sym: List[str]
    _id: int
    _sym_locations: Dict[str, LocationMark]

    def __init__(self, pdb_id: str, chain: str, _id: int, is_custom_file: bool = False):
        super().__init__(pdb_id, chain, is_custom_file=is_custom_file)
        self._sym_locations = {}
        self._id = _id
        self._list_sym = []

    def get_id(self) -> int:
        return self._id

    def extend(self, pymol_inst: pymol2.PyMOL) -> None:
        transform = pymol_inst.cmd.get_object_matrix(str(self))
        sim_full = self.pdb_id
        self.load_in_pymol(pymol_inst, sim_full, is_full=True)
        if not settings.FIND_ONLY_IN_SELF_CHAIN and ('DIFFRACTION' in self._structure_method):
            if self.pdb_id not in settings.bad_symexp_list:
                pymol_inst.cmd.symexp("sym", sim_full, sim_full, settings.SYMEXP_GENERATION_RADIUS)

        pymol_inst.cmd.transform_selection(f'({sim_full} or sym*)', transform)
        obj_list = pymol_inst.cmd.get_object_list("sym*")

        pymol_remove_far(pymol_inst, sim_full, settings.VOLUME_OBJ, settings.SYMMETRY_CUTOFF)
        for obj in obj_list:
            pymol_remove_far(pymol_inst, obj, settings.VOLUME_OBJ, settings.SYMMETRY_CUTOFF)
            # Delete object if it Empty
            if pymol_inst.cmd.select(obj) != 0:
                self._list_sym.append(obj)
            else:
                pymol_inst.cmd.delete(obj)

        if len(self._list_sym) > settings.SIM_LIST_SIZE_ERROR_IDENTIFIER:
            self._list_sym = []
        self._list_sym.insert(0, sim_full)
        for i, obj in enumerate(self._list_sym):
            if i == 0:
                location = LocationMark.not_symmetry
            else:
                location = LocationMark.symmetry
            self._sym_locations[obj] = location

    def get_sym_list(self) -> List[str]:
        if len(self._list_sym) == 0:
            raise Warning("Use SimilarObject.extend() first")
        else:
            return self._list_sym

    def get_sym_locations(self) -> Dict[str, LocationMark]:
        if len(self._sym_locations) == 0:
            raise Warning("Use SimilarObject.extend() first")
        else:
            return self._sym_locations

    def load_in_pymol(self, pymol_inst: pymol2.PyMOL, obj_name: Optional[str] = None, is_full: bool = False) -> None:
        chain_to_load = self.chain
        if is_full:
            chain_to_load = None
        if obj_name is None:
            obj_name = self.get_pymol_name()
        if self.custom_file_path is None:
            path_cif = load_object(pymol_inst, obj_name, chain=chain_to_load, pdb_id=self.pdb_id)
        else:
            path_cif = load_object(pymol_inst, obj_name, path=self.custom_file_path)
        self.set_path_cif(path_cif)

    def find_residues_in_volume(self, pymol_inst: pymol2.PyMOL, volume_obj: str, volume_radius: float):
        map_obj = {}
        obj_list_to_search = self.get_sym_list()
        for obj in obj_list_to_search:
            map_resn = {}
            for atom in pymol_inst.cmd.get_model(f'({obj}) within {volume_radius} of ({volume_obj})').atom:
                key = (atom.resi, atom.chain)
                map_resn[key] = atom.resn

            map_obj[obj] = [[chain, resi, map_resn[(resi, chain)]] for resi, chain in map_resn.keys()]

        result = []
        for obj, res in map_obj.items():
            for chain, resi, resn in res:
                locations = self.get_sym_locations()
                location_mark = ResidueObject.refine_location(locations[obj], chain, self)
                result.append(ResidueObject(pymol_inst, self.get_full_name(), obj, chain, resn, resi, location_mark))

        return result
