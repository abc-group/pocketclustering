import os
import shutil
import tempfile
from typing import Dict, AnyStr

import settings

from pocketclustering.utils import render_template, escape_chars


def init_folder(fld_name: str) -> str:
    if not os.path.exists(fld_name):
        os.mkdir(fld_name)
    else:
        if not os.path.isdir(fld_name):
            raise Exception(f'Cannot create folder {fld_name}')
    return fld_name


def make_output_suffix(smarts_catalog: str) -> str:
    flags_string = os.path.splitext(os.path.basename(smarts_catalog))[0]
    if not settings.FIND_IN_PROTEIN:
        flags_string += '_np'
    if not settings.FIND_IN_LIGAND:
        flags_string += '_nl'
    if settings.FIND_ONLY_IN_SELF_CHAIN:
        flags_string += '_selfchain'
    if settings.NOT_FIND_IN_SELF_CHAIN:
        flags_string += '_notselfchain'

    return flags_string


class MinimizationCase:
    def __init__(self, case_name: str):
        min_tmp_fld = file_manager.get_folder_minimization_sessions()
        self.work_dir = os.path.join(os.getcwd(), min_tmp_fld, case_name)
        os.makedirs(self.work_dir, exist_ok=True)

        self.minimization_in = os.path.join(self.work_dir, 'min.in')
        self.minimization_out = os.path.join(self.work_dir, 'min.out')
        self.logfile = os.path.join(self.work_dir, 'log.txt')

        self.complex_pdb = os.path.join(self.work_dir, 'complex.pdb')
        self.complex_pdb_param = os.path.join(self.work_dir, 'complex_paramed.pdb')
        self.cp_prmtop = os.path.join(self.work_dir, 'complex.prmtop')
        self.cp_rst7 = os.path.join(self.work_dir, 'complex.rst7')
        self.cp_ncrst = os.path.join(self.work_dir, 'complex_min.ncrst')
        self.tleap_complex_in = os.path.join(self.work_dir, 'tleap_complex.in')

        self.fg_mol2 = os.path.join(self.work_dir, 'ligand.mol2')
        self.fg_frcmod = os.path.join(self.work_dir, 'ligand.frcmod')
        self.fg_lib = os.path.join(self.work_dir, 'fg.lib')
        self.fg_prmtop = os.path.join(self.work_dir, 'ligand.prmtop')
        self.fg_rst7 = os.path.join(self.work_dir, 'ligand.rst7')
        self.fg_pdb_with_h = os.path.join(self.work_dir, 'fgroup.mol')
        self.fg_charged = os.path.join(self.work_dir, 'ligand_gascharged.mol2')
        self.fg_before = os.path.join(self.work_dir, 'before.pdb')
        self.fg_minimized = os.path.join(self.work_dir, 'minimized.pdb')
        self.tleap_fgroup_in = os.path.join(self.work_dir, 'tleap_ligand.in')

        self.final_pdb = os.path.join(self.work_dir, 'final.pdb')

    def as_dict(self) -> Dict:
        return {f'{key}_path': value for key, value in self.__dict__.items() if not key.startswith('__') and not callable(key)}


class MinimizationTestEnvironment:
    work_dir: AnyStr
    tleap_complex_in: AnyStr
    pdb_for_test: AnyStr

    def __init__(self, test_case_name: str):
        pymol_obj = escape_chars(test_case_name)
        min_tmp_fld = file_manager.get_folder_minimization_sessions()
        self.work_dir = os.path.join(os.getcwd(), min_tmp_fld, f'test_{pymol_obj}')
        self.tleap_complex_in = os.path.join(self.work_dir, 'tleap_complex_test.in')
        self.pdb_for_test = os.path.join(self.work_dir, 'test.pdb')

    def __enter__(self):
        os.makedirs(self.work_dir, exist_ok=True)
        render_template(os.path.join(settings.MINIMIZATION_DATA_FOLDER, 'tleap_complex_test.in'), self.tleap_complex_in, pdb=self.pdb_for_test)

        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        shutil.rmtree(self.work_dir)


class FileManager:
    def __init__(self, pdb_folder):
        self.fld_res = init_folder('residues')
        self.fld_output = init_folder('output')
        self.fld_align_data = init_folder('align_data')
        self.fld_cif = init_folder(pdb_folder)
        self.fld_minimization_sessions = init_folder('minimize_sessions')
        self.fld_minimization_data = settings.MINIMIZATION_DATA_FOLDER
        self.fld_amber_bins = os.path.join(settings.AMBER_FOLDER, 'bin')

        self.template_fgroup = os.path.join(self.fld_minimization_data, 'tleap_ligand_template.in')
        self.template_complex = os.path.join(self.fld_minimization_data, 'tleap_complex_template.in')
        self.template_minimization = os.path.join(self.fld_minimization_data, 'min.in')

    def get_folder_cif(self) -> str:
        return self.fld_cif

    def get_folder_align_data(self) -> str:
        return self.fld_align_data

    def get_folder_minimization_sessions(self) -> str:
        return self.fld_minimization_sessions

    def get_folder_minimization_data(self) -> str:
        return self.fld_minimization_data

    def get_folder_amber_bins(self) -> str:
        return self.fld_amber_bins

    def get_output_file(self, name: str, suffix: str) -> str:
        return f'{self.fld_output}/{name}{suffix}'

    def get_mol_file_residue(self, name: str, origin: str, res_obj_verbose_id) -> str:
        fld = init_folder(f'{self.fld_res}/{origin}')
        return f'{fld}/{name}_{res_obj_verbose_id}.mol'

    @staticmethod
    def name_from_patch(name: str) -> str:
        return os.path.splitext(os.path.basename(name))[0]

    @staticmethod
    def create_temp_file(suffix):
        tmp_file = tempfile.NamedTemporaryFile(delete=False, suffix=suffix)
        return tmp_file.name


file_manager = FileManager(settings.PATH_TO_PDB_DATABASE)
